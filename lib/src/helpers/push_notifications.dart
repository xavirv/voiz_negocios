import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotification {

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  initNotifications(){
    _firebaseMessaging.requestNotificationPermissions();

    _firebaseMessaging.getToken().then((token) {
      print('=====FCM TOEKN=====');
      print(token);
      //
    });

    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message){
          print('==== ON MESSAGE =====');
          print(message);
        },
        onLaunch: (Map<String, dynamic> message){
          print('==== ON RESTART =====');
          print(message);
        },
        onResume: (Map<String, dynamic> message){
          print('==== ON RESUME =====');
          print(message);
        }
    );
  }
}