import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:html/parser.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:http/http.dart' as http;
import 'package:voiz.negocios/src/elements/CircularLoadingWidget.dart';
import '../../generated/l10n.dart';
import '../models/order.dart';
import '../repository/order_repository.dart';
import '../controllers/order_controller.dart';

class OrderDetailsController extends ControllerMVC {
  Order order;
  OrderController _con;
  static var status2 = "";
  static var Status_Order = "";
  static var StatusSeguimiento = "";
  static bool pass = true;
  GlobalKey<ScaffoldState> scaffoldKey;

  OrderDetailsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void status_order(status)
  {
     if(status == "Orden recibida") {
       Status_Order = "¿Aceptar orden?";
       StatusSeguimiento = "Cambiar el estado de la orden a \"Aceptada\"";
       status2="Orden recibida";}
     if(status == "Aceptada") {
       Status_Order = "¿Pasar a preparar?";
       StatusSeguimiento = "Cambiar el estado de la orden a \"Preparando\"";
       status2="Aceptada";}
     if(status == "Preparando") {
       Status_Order = "¿Pasar a lista?";
       StatusSeguimiento = "Cambiar el estado de la orden a \"Lista\"";
       status2="Preparando";}
     if(status == "Lista") {
       Status_Order = "¿Entregar a repartidor?";
       StatusSeguimiento = "Cambiar el estado de la orden a \"En camino\"";
       status2="Lista";}
     if(status == "En camino") {
       Status_Order = "";
       status2="En camino"; }
  }

  void listenForOrder({String id, String message}) async {
    final Stream<Order> stream = await getOrder(id);
    stream.listen((Order _order) {
      if(_order.accepted_order_status == "Aceptada")
        {
          if(_order.orderStatus.id == 6 || _order.orderStatus.id == null)
          {
            _order.orderStatus.status = "Aceptada";
          }
          if(_order.orderStatus.id == 7)
          {
            _order.orderStatus.status = "No Aceptada";
          }
          if(_order.orderStatus.status != null)
            {
              status_order(_order.orderStatus.status);
            }
          setState(() => order = _order);

        }
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S
            .of(context)
            .verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshOrder() async {
    listenForOrder(
        id: order.id, message: S
        .of(context)
        .order_refreshed_successfuly);
  }

  void doAcceptOrder(Order _order) async {

    accepted(_order).then((value) {
      setState(() {
        this.order.orderStatus.id = '6';
        this.order.orderStatus.status = 'Aceptada';
      });
      var scaffoldKey;
      var Msg = 'La orden #'+_order.id+' ha sido aceptada!';
      var body = 'La orden #'+_order.id+' ha sido aceptada!';
      var title = 'La orden #'+_order.id+' ha sido aceptada!';
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text('The order has been accepted'),
      ));
      obtener_Token(body,title,Msg,_order.driver_id);
      obtener_Token(body,title,Msg,_order.user.id);
      status_order("Aceptada");
    });
  }

  void doRejectOrder(Order _order) async {

    reject(_order).then((value) {
      setState(() {
        this.order.orderStatus.id = '7';
        this.order.orderStatus.status = 'No Aceptada';
      });
      var scaffoldKey;
      var Msg = 'La orden #'+_order.id+' ha sido canelada!';
      var body = 'La orden #'+_order.id+' ha sido canelada!';
      var title = 'La orden #'+_order.id+' ha sido canelada!';
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text('The order has been refused'),
      ));
      obtener_Token(body,title,Msg,_order.driver_id);
      obtener_Token(body,title,Msg,_order.user.id);
    });
  }

  void doPreparingOrder(Order _order) async {

    preparing(_order).then((value) {
      setState(() {
        this.order.orderStatus.id = '2';
        this.order.orderStatus.status = "Preparando";
      });
      var scaffoldKey;

      var Msg = 'La orden #'+_order.id+' esta siendo preparada!';
      var body = 'La orden #'+_order.id+' esta siendo preparada!';
      var title = 'La orden #'+_order.id+' esta siendo preparada!';

      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text('The order is being prepared'),
      ));
      obtener_Token(body,title,Msg,_order.driver_id);
      obtener_Token(body,title,Msg,_order.user.id);
      status_order("Preparando");
    });
  }

  void doReadyOrder(Order _order) async {

    ready(_order).then((value) {
      setState(() {
        this.order.orderStatus.id = '3';
        this.order.orderStatus.status = "Lista";
      });
      var scaffoldKey;

      var Msg = 'La orden #'+_order.id+' esta lista!';
      var body = 'La orden #'+_order.id+' esta lista!';
      var title = 'La orden #'+_order.id+' esta lista!';

      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text('The order is ready'),
      ));
      obtener_Token(body,title,Msg,_order.driver_id);
      obtener_Token(body,title,Msg,_order.user.id);
      status_order("Lista");
    });
  }

  void doInWayOrder(Order _order) async {
    inWay(_order).then((value) {
      setState(() {
        this.order.orderStatus.id = '4';
        this.order.orderStatus.status = "En camino";
      });
      var scaffoldKey;
      var Msg = 'La orden #'+_order.id+' esta en camino!';
      var body = 'La orden #'+_order.id+' esta en camino!';
      var title = 'La orden #'+_order.id+' esta en camino!';
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(Msg),
      ));
      obtener_Token(body,title,Msg,_order.driver_id);
      obtener_Token(body,title,Msg,_order.user.id);
      status_order("En camino");
    });
  }
  StreamSubscription<DocumentSnapshot> Documentsubscription;

  obtener_Token(body,title,Msg,Id_User) async
  {
   var Token="";
    Documentsubscription = Firestore.instance
       .collection('Token_Device')
       .document(Id_User)
       .snapshots()
       .listen((event) {
        Token = event.data["Token"];
         FCM_Notification(body,title,Msg,Token);
       });
  }

  void FCM_Notification(body,title,Msg,Token) async
  {

    message['to'] = Token;
    Notification['body'] = body;
    Notification['title'] =title;
    http.Response r = await http.post(url, headers: headers, body: jsonEncode(message));
    print(r.statusCode);
    print(r.body);
    Documentsubscription.cancel();
  }
  final String url = 'https://fcm.googleapis.com/fcm/send';
  static String key = 'key=AAAAKqR3-qo:APA91bFan5JsbGBWw_xdwyVVeVuevX8QUfxFX4AZIO3pULohGoOsRNMvWV3ihQoFvPRO044D74Vi5SxewWhjWYw_h5cMcR0LWgr6QjPfqqsvu6s2HsRatr8X29jLJReYARX2a7f0ccQt';

  Map<String, String> headers = {
    'Authorization': key,
    'content-type': 'application/json',
  };

  Map<String, dynamic> message = {
    'notification': Notification,
    'priority': 'high',
    'data': data,
    'android': android,
    'apns': apns,
    'to': '', // this is optional - used to send to one device
  };
  static Map<String, dynamic> Notification = {
    'body': '',
    'title': ''
  };
  static Map<String, dynamic> data = {
    'click_action': 'FLUTTER_NOTIFICATION_CLICK',
    'sound': 'default',
  };
  static Map<String, dynamic> android = {
  'prioty': 'high',
  'sound': 'default',
   "ttl":"4500s"
  };
  static Map<String, dynamic> apns = {
    'headers':headresApns
  };
  static Map<String, dynamic> headresApns = {
    'apns-priority': '10'
  };
  static Map<String, dynamic> webpush = {
    'headers': '10'
  };
  static Map<String, dynamic> headresWebPush = {
    "Urgency": "high",
    "TTL":"4500"
  };

}

