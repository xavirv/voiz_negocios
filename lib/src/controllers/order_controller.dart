import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/order.dart';
import '../models/user.dart';
import '../repository/order_repository.dart';
import '../repository/user_repository.dart';


class OrderController extends ControllerMVC {
  List<Order> orders = <Order>[];
  GlobalKey<ScaffoldState> scaffoldKey;

  User user = new User();


  OrderController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  @override
  void initState(){
    TiempoReal();
    super.initState();
  }

  void TiempoReal({String message}) async {
    final Stream<Order> stream = await getOrders();
    stream.listen((Order _order) {
      setState(() {
        if(_order.foodOrders[0].food.restaurant.id == currentUser.value.restaurante_id.toString()) {
          if(_order.acceptedOrderStatus == "Aceptada" && _order.orderStatus.id != '7') {
            if(_order.orderStatus.id == null)
              {
                _order.orderStatus.status = "Aceptada";
              }
            orders.add(_order);
          }
        }
      });
    },  onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
    tiempo();
  }

  void listenForOrders({String message}) async {
    final Stream<Order> stream = await getOrders();
    stream.listen((Order _order) {
      setState(() {
        if(_order.foodOrders[0].food.restaurant.id == currentUser.value.restaurante_id.toString()) {
          if (_order.acceptedOrderStatus == "Aceptada" && _order.acceptedOrderStatus == "Acceptada" && _order.orderStatus.id != '7') {
            orders.add(_order);
          }
        }
      });
    },  onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
    tiempo();
  }

  void listenForOrdersHistory({String message}) async {
    final Stream<Order> stream = await getOrdersHistory();
    stream.listen((Order _order) {
      setState(() {
        if(_order.foodOrders[0].id == currentUser.value.restaurante_id.toString())
        {
          orders.add(_order);
        }
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }

  Future<void> refreshOrdersHistory() async {
    orders.clear();
    listenForOrdersHistory(message: S.of(context).order_refreshed_successfuly);
  }

  Future<void> refreshOrders() async {
    orders.clear();
    listenForOrders(message: S.of(context).order_refreshed_successfuly);
    tiempo();
  }

  Future<void> tiempo() async {
    orders.clear();
    Future.delayed(Duration(seconds: 5), () {

      TiempoReal();
    });

  }
}

