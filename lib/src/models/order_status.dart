import '../helpers/custom_trace.dart';

class OrderStatus {
  String id;
  String status;

  OrderStatus();


  OrderStatus.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      id = jsonMap['id'].toString();
      status = jsonMap['order_status'] != null ? jsonMap['order_status'] : '';
    } catch (e) {
      id = '';
      status = '';
      print(CustomTrace(StackTrace.current, message: e));
    }
  }
}
